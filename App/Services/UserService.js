import {ApiConfig} from '../ApiConfig';
import {getItemFromStorage} from '../Utils/Storage';
import axios from 'axios';

export const isLoggedIn = async () => {
  const token = await getItemFromStorage('token');
  if (!token) {
    return false;
  }
  ApiConfig.token = token;
  return true;
};

export const userLogin = async params => {
  const response = await axios.post(ApiConfig.login, params);
  return response.data;
};

// export const randomdata = async params => {
//   const url = ApiConfig.random + params;
//   console.log(url, params);
//   const response = await axios.get(url);
//   return response.data;
// };

export const randomdata = async params => {
  const page = params?.page;
  const gender = params?.gender;
  // console.log('page', page);

  const url = `${ApiConfig.random}&page=${page}&gender=${gender}&results=20`;
  const response = await axios.get(url);
  return response.data;
};
