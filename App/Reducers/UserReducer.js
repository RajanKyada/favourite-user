import {
  RANDOM_GET,
  RANDOM_SET,
  SET_USER,
  LIKE_USER,
  UNLIKE_USER,
  LOADMORE_DATA,
  FILTER_USER,
} from '../Actions/Keys';
import DefaultState from './Default';

const INIT_STATE = DefaultState.user;

const UserReducer = (state = INIT_STATE, action) => {
  switch (action.type) {
    case SET_USER:
      return {...state, user: action.payload};
    case RANDOM_GET:
      return {...state, data: state?.data, currentpage: action.payload.page};
    case RANDOM_SET:
      return {...state, data: action.payload};
    case LIKE_USER:
      return {...state, likeuser: [...state.likeuser, action.payload]};
    case UNLIKE_USER:
      return {
        ...state,
        likeuser: state.likeuser.filter(item => item !== action.payload),
      };
    case FILTER_USER:
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default UserReducer;
