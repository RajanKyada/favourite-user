import {takeLatest} from 'redux-saga/effects';
import {FILTER_USER, GET_USER, RANDOM_GET} from '../Actions/Keys';
import {getFilterUserSaga, getRandomUserSaga, getUserSaga} from './UserSaga';

export default function* rootSaga() {
  // yield takeLatest(GET_USER, getUserSaga);
  yield takeLatest(RANDOM_GET, getRandomUserSaga);

  yield takeLatest(FILTER_USER, getFilterUserSaga);
}
