// import {put} from 'redux-saga/effects';
import {put, select} from 'redux-saga/effects';
import {RANDOM_SET} from '../Actions/Keys';
import {randomdata} from '../Services/UserService';

const getRandomdata = state => state.user.data || [];

export function* getRandomUserSaga(action) {
  const randomList = yield select(getRandomdata);
  // console.log('Listr', randomList);
  try {
    const page = action.payload?.page;
    const params = {
      page,
      gender: action.payload?.gender,
    };
    console.log(params);
    const response = yield randomdata(params);
    // console.log(response?.results.length);
    if (response?.results) {
      let data = response.results || [];

      console.log('rwe', response);
      if (page) {
        const oldList = randomList;
        data = [...oldList, ...response.results];
      }
      // if (action.payload.gender && action.payload.page === '1') {
      //   data = response.results;
      // } else {

      // }
      yield put({
        type: RANDOM_SET,
        payload: data,
      });
    }
  } catch (error) {
    console.log(error);
  }
}

export function* getFilterUserSaga(action) {
  try {
    const page = action.payload?.page;
    const params = {
      page,
      gender: action.payload?.gender,
    };
    const response = yield randomdata(params);
    // console.log(response?.results.length);
    if (response?.results) {
      let data = response.results || [];

      if (action.payload.gender && action.payload.page === '1') {
        data = response.results;
      }
      yield put({
        type: RANDOM_SET,
        payload: data,
      });
    }
  } catch (error) {
    console.log(error);
  }
}
