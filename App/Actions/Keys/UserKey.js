export const GET_USER = 'GET_USER';
export const SET_USER = 'SET_USER';

export const LOG_OUT = 'LOG_OUT';

export const RANDOM_GET = 'RANDOM_GET';
export const RANDOM_SET = 'RANDOM_SET';

export const LIKE_USER = 'LIKE_USER';

export const UNLIKE_USER = 'UNLIKE_USER';

export const FILTER_USER = 'FILTER_USER';
