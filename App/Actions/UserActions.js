import {
  GET_USER,
  LOG_OUT,
  RANDOM_GET,
  LIKE_USER,
  UNLIKE_USER,
  FILTER_USER,
} from './Keys';

export const getUserDetail = () => ({
  type: GET_USER,
});

export const userLogout = () => ({
  type: LOG_OUT,
});

export const randomData = payload => ({
  type: RANDOM_GET,
  payload,
});

export const LikeUser = payload => ({
  type: LIKE_USER,
  payload,
});

export const UnlikeUser = payload => ({
  type: UNLIKE_USER,
  payload,
});

export const filterUser = payload => ({
  type: FILTER_USER,
  payload,
});
