import React, {useContext, useEffect} from 'react';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useNavigation} from '@react-navigation/native';

import CommonStyle from '../../../Theme/CommonStyle';
import {AppContext} from '../../../AppContext';
import {useSelector, useDispatch} from 'react-redux';
import {CustomText} from '../../CommonComponent';
import {ButtonComponent} from '../../SubComponents';
import {UnlikeUser} from '../../../Actions/UserActions';

const styles = StyleSheet.create({
  btnTitle: {
    marginVertical: 10,
  },
  btnBorder: {
    borderRadius: 40,
  },
});

const Users = props => {
  const {appTheme} = useContext(AppContext);
  const {btnTitle, btnBorder} = styles;

  const {likeuser} = useSelector(state => state.user);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  return (
    <SafeAreaView
      style={[
        CommonStyle.flexContainer,
        // CommonStyle.center,
        {backgroundColor: appTheme.background},
      ]}>
      <ScrollView>
        {likeuser?.map((item, index) => {
          return (
            <View key={index}>
              <TouchableOpacity
                style={{flexDirection: 'row', margin: 10}}
                onPress={() => navigation.navigate('UserInfo', {item})}>
                <Image
                  source={{uri: item.picture?.medium}}
                  style={{width: 80, height: 80, borderRadius: 40}}
                />
                <View style={{margin: 10}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text>{item.name?.title}&nbsp;</Text>
                    <Text>{item.name?.first}&nbsp;</Text>
                    <Text>{item.name?.last}</Text>
                  </View>

                  <View style={{flexDirection: 'row', marginTop: 10}}>
                    <Text style={{fontWeight: 'bold'}}>Age :</Text>
                    <Text>&nbsp;{item.dob?.age}</Text>
                  </View>
                </View>
                <View style={{position: 'absolute', right: 10, marginTop: 10}}>
                  <TouchableOpacity onPress={() => dispatch(UnlikeUser(item))}>
                    <AntDesign name="heart" size={25} color="#deb110" />
                  </TouchableOpacity>
                </View>
              </TouchableOpacity>
            </View>
          );
        })}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Users;
