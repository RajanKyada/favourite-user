import React, {useContext, useEffect, useState} from 'react';
import {
  SafeAreaView,
  FlatList,
  Text,
  View,
  Image,
  TouchableOpacity,
  Button,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Modal from 'react-native-modal';
import {useSelector, useDispatch} from 'react-redux';
import {CustomText} from '../../CommonComponent';
import CommonStyle from '../../../Theme/CommonStyle';
import {AppContext} from '../../../AppContext';
import {LikeUser, randomData, filterUser} from '../../../Actions/UserActions';
import UserComponent from './UserComponent';

const Home = props => {
  const {appTheme} = useContext(AppContext);

  const {data, currentpage} = useSelector(state => state.user);

  // ======

  const [select, setSelect] = useState();

  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  // =====

  // console.log('data', data);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(randomData({page: 1, gender: select}));
  }, []);

  const loadMore = () => {
    dispatch(randomData({page: currentpage + 1, gender: select}));
  };

  const renderItem = ({item, index}) => {
    return (
      <View>
        <UserComponent item={item} />
      </View>
    );
  };

  return (
    <SafeAreaView
      style={[
        CommonStyle.flexContainer,
        // CommonStyle.center,
        {backgroundColor: appTheme.background},
      ]}>
      {/* Model */}
      <View style={{flex: 1}}>
        {/* <Button title="Show modal" onPress={toggleModal} /> */}

        <Modal isVisible={isModalVisible}>
          <View
            style={{
              backgroundColor: '#fff',
              // height: 150,
              alignItems: 'center',
              justifyContent: 'center',
              padding: 20,
            }}>
            <View style={{marginBottom: 30}}>
              <TouchableOpacity
                style={{
                  marginTop: 10,
                  width: 180,
                  backgroundColor: select === 'male' ? '#FAFAFA' : null,
                  alignItems: 'center',
                }}
                onPress={() => setSelect('male')}>
                <Text style={{fontSize: 18}}>Male</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  marginTop: 10,
                  width: 180,
                  backgroundColor: select === 'female' ? '#FAFAFA' : null,
                  alignItems: 'center',
                }}
                onPress={() => setSelect('female')}>
                <Text style={{fontSize: 18}}>Female</Text>
              </TouchableOpacity>
            </View>

            <Button
              title="Ok"
              onPress={() => {
                setModalVisible(!isModalVisible),
                  dispatch(filterUser({page: 1, gender: select}));
              }}
            />
          </View>
        </Modal>
      </View>
      {/* ===== */}
      <TouchableOpacity
        style={{
          marginBottom: 5,
          alignSelf: 'flex-end',
          marginRight: 15,
        }}
        // onPress={() => dispatch(randomData({page: 1, gender: 'female'}))}>
        onPress={toggleModal}>
        <Image
          source={require('../../../../Images/Share.png')}
          resizeMode="contain"
          style={{
            width: 20,
            height: 20,
          }}
        />
      </TouchableOpacity>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={data}
        keyExtractor={item => item.login?.uuid}
        renderItem={renderItem}
        onEndReached={loadMore}
        onEndThreshold={0.3}
      />
    </SafeAreaView>
  );
};

export default Home;
