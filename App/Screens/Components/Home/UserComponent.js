import React, {useEffect, useState} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useSelector, useDispatch} from 'react-redux';
import {LikeUser, randomData, UnlikeUser} from '../../../Actions/UserActions';

export default function UserComponent({item}) {
  if (!item) return null;

  const {likeuser} = useSelector(state => state.user);
  const navigation = useNavigation();

  const dispatch = useDispatch();

  const isFav = likeuser.filter(res => res?.email === item?.email)?.length;

  return (
    <TouchableOpacity
      style={{flexDirection: 'row', margin: 10}}
      onPress={() => navigation.navigate('UserInfo', {item})}>
      <Image
        source={{uri: item.picture?.medium}}
        style={{width: 80, height: 80, borderRadius: 40}}
      />
      <View style={{margin: 10}}>
        <View style={{flexDirection: 'row'}}>
          <Text>{item.name?.title}&nbsp;</Text>
          <Text>{item.name?.first}&nbsp;</Text>
          <Text>{item.name?.last}</Text>
        </View>

        <View style={{flexDirection: 'row', marginTop: 10}}>
          <Text style={{fontWeight: 'bold'}}>Age :</Text>
          <Text>&nbsp;{item.dob?.age}</Text>
        </View>
      </View>
      <View style={{position: 'absolute', right: 10, marginTop: 10}}>
        <TouchableOpacity
          onPress={() => {
            !isFav ? dispatch(LikeUser(item)) : dispatch(UnlikeUser(item));
          }}>
          <AntDesign
            name={isFav ? 'heart' : 'hearto'}
            size={25}
            color="#deb110"
          />
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
}
