import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  Image,
  StyleSheet,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Fontisto from 'react-native-vector-icons/Fontisto';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default function Userinfo(props) {
  const {item} = props?.route?.params;
  // console.log('nv', item);
  const navigation = useNavigation();

  return (
    <View>
      <TouchableOpacity
        style={{position: 'absolute', margin: 15, zIndex: 999}}
        onPress={() => navigation.goBack()}>
        <MaterialIcons name="arrow-back-ios" size={20} color="#fff" />
      </TouchableOpacity>
      <ImageBackground
        source={{
          uri: 'https://img.freepik.com/free-vector/dark-low-poly-background_1048-7971.jpg?size=626&ext=jpg',
        }}
        style={{width: WIDTH, height: HEIGHT / 4}}>
        <View style={{justifyContent: 'center', alignSelf: 'center'}}>
          <Image
            source={{uri: item.picture?.medium}}
            style={{width: 120, height: 120, borderRadius: 60, marginTop: 100}}
          />
        </View>
        <View style={{alignItems: 'center', marginTop: 20}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={style.usename}>{item.name?.title}&nbsp;</Text>
            <Text style={style.usename}>{item.name?.first}&nbsp;</Text>
            <Text style={style.usename}>{item.name?.last}</Text>
          </View>
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
            <Octicons name="location" size={15} color="#000" />

            <Text>&nbsp;&nbsp;{item.location.street?.number}</Text>
            <Text>&nbsp;{item.location.street?.name}</Text>
          </View>
        </View>
        <View style={{margin: 10}}>
          <View style={style.fold1}>
            <Entypo name="email" size={18} color="#000" />
            <Text>&nbsp;{item?.email}</Text>
          </View>
          {/* coutry */}
          <View style={style.fold1}>
            <Ionicons name="ios-locate" size={18} color="#000" />
            <Text>&nbsp;{item.location?.country}</Text>
          </View>
          {/* call */}
          <View style={style.fold1}>
            <Entypo name="old-phone" size={18} color="#000" />
            <Text>&nbsp;{item?.phone}</Text>
          </View>
          {/* Date */}
          <View style={style.fold1}>
            <Fontisto name="date" size={18} color="#000" />
            <Text>&nbsp;{item.dob?.date.split('T')[0]}</Text>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
}

const style = StyleSheet.create({
  usename: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  fold1: {
    flexDirection: 'row',
    marginTop: 20,
    alignItems: 'center',
  },
});
